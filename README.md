FizzBuzz
========

The program in src/fizzbuzz.py does implement the following:

- Go through numbers 1 to n (including).

- If number is divisible by 5 (and not by 7) print “Fizz”.

- If number is divisible by 7 (and not by 5) print “Buzz”.

- If number is divisible by 5 and 7 print “FizzBuzz”.

- For all other numbers, print the number.

Used technologies: python 3.6.9.

Setup a project:
==============

- clone a project

- go to the project folder

 -run next commands in terminal:
 
```bash
python3 -m venv venv
source venv/bin/activate 
```

Run a program:
```bash
python src/fizzbuzz.py 
```

 
