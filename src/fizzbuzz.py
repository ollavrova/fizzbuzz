def fizzbuzz(n: int):
    for i in range(1, n+1):
        if i % 5 == 0 and i % 7 == 0:
            print("FizzBuzz")
        if i % 5 == 0:
            print("Fizz")
        elif i % 7 == 0:
            print("Buzz")
        else:
            print(i)


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description="FizzBuzz")
    parser.add_argument('-n', type=int, default=100, help="Print up to this number")
    args = parser.parse_args()

    fizzbuzz(args.n)
